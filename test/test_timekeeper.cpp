#include <Arduino.h>
#include <unity.h>
#include <timekeeper.h>

void test_parse_time(){
    tm time = {.tm_year=122};
    parse_dtime(&time, "20220926", "1058");
    TEST_ASSERT_TRUE(time.tm_year == 122);
    TEST_ASSERT_TRUE(time.tm_mon == 8);
    TEST_ASSERT_TRUE(time.tm_mday == 26);
    TEST_ASSERT_TRUE(time.tm_hour == 10);
    TEST_ASSERT_TRUE(time.tm_min == 58);
}

void setup() {
    UNITY_BEGIN();

    RUN_TEST(test_parse_time);

    UNITY_END();
}

void loop() {}
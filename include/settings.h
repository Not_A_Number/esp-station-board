/*
 * Individual Settings
 */
const char* ssid = "Freifunk";              // SSID (Wifi-Name)
const char* password = "";                  // Wifi Password
const char* station_id = "8070146";         // Station id, retrieve from py/getStationId.py

/*
 * Further Settings
 */
#define MIN_MINUTES_UNTIL_DEP 1             // Minimum Minutes until departure for the Train/Bus to be displayed
#define NUM_DISPLAYS 3                      // Number of available displays (not matrices)
#define NUM_REQUESTED (NUM_DISPLAYS * 3)    // Number of requested departures
#define UPDATE_INTERVAL_MIN 3               // Describes how often is the server polled for next departures

/*
 * Hardware Setup
 */
#define HARDWARE_TYPE MD_MAX72XX::FC16_HW
#define MAX_DEVICES (NUM_DISPLAYS * 4)
#define DATA_PIN  D4
#define CS_PIN    D3
#define CLK_PIN   D2
#include <Arduino.h>
#include <cstring>
#include <MD_MAX72xx.h>
#include <utf8.h>

#include "settings.h"
#include <transporter.h>
#include <timekeeper.h>
#include <requester.h>
#include <ledmapper.h>


transporter trans[NUM_REQUESTED];
tm updated_time = {.tm_year=122}; //Start year 2022
time_t last_update_seconds = 0;

requester req = requester(ssid, password, station_id, NUM_REQUESTED, trans, &updated_time, &last_update_seconds);
uint8_t bmp[8 * 4 * NUM_DISPLAYS];

MD_MAX72XX mx = MD_MAX72XX(HARDWARE_TYPE, DATA_PIN, CLK_PIN, CS_PIN, MAX_DEVICES);

void updateBmp(int answers){
  for (size_t i = 0; i < sizeof(bmp); i++) {bmp[i] = 0;}  // Empty bitmap

  size_t display_i = 0;
  for (size_t i = 0; display_i < NUM_DISPLAYS && i < answers; i++)
  {
    double minutes_double = difftime(trans[i].time, mktime(&updated_time)) / 60;

    // Skip trans[i] if already departed
    if (minutes_double < MIN_MINUTES_UNTIL_DEP){
      continue;
    }

    uint8 beginning_i = display_i * 32;
    uint8 end_i = display_i * 32 + 31;
    utf8_int32_t c;
    char* next;

    // Draw line number
    next = trans[i].name;
    next = utf8codepoint(next, &c);
    while (c != '\0')
    {
      for (size_t k = 0; k < getFontLength(&c); k++)
      {
        for (size_t column_c = 0; column_c < 8; column_c++)
        {
          bmp[beginning_i] |= ((getFontColumn(&c, column_c) >> k) & 1) << column_c;
        }
        beginning_i++;
      }
      next = utf8codepoint(next, &c);
    }
    beginning_i++;

    // Draw minutes
    char minutes_str[10];
    itoa(minutes_double, minutes_str, 10);
    for (int8 j = strlen(minutes_str) - 1; j >= 0; j--)
    {
      c = minutes_str[j];
      for (int8 k = getFontLength(&c) - 1; k >= 0; k--)
      {
        for (size_t column_c = 0; column_c < 8; column_c++)
        {
          bmp[end_i] |= ((getFontColumn(&c, column_c) >> k) & 1) << column_c;
        }
        end_i--;
      }
    }
    end_i--;

    // Draw direction
    next = trans[i].direction;
    next = utf8codepoint(next, &c);
    while (getFontLength(&c) <= end_i - beginning_i && c != '\0')
    {
      for (size_t k = 0; k < getFontLength(&c); k++)
      {
        //TODO: unreasonable expense of looking up bitmap 8 times, char should be cached
        for (size_t column_c = 0; column_c < 8; column_c++)
          bmp[beginning_i] |= ((getFontColumn(&c, column_c) >> k) & 1) << column_c;
        beginning_i++;
      }
      next = utf8codepoint(next, &c);
    }

    // Rotate, if display_i uneqal
    if (display_i % 2 == 1)
      for (size_t i = 0; i < 16; i++)
      {
        uint8_t *first = &bmp[display_i * 32 + i];
        uint8_t *last = &bmp[display_i * 32 + 31 - i];
        uint8_t tmp = *first;
        *first = reverse_bitorder[*last];
        *last = reverse_bitorder[tmp];
      }
    display_i++;
  }
}

void updateDisplays(int answers){
  update_time_offline(&updated_time, &last_update_seconds);

  updateBmp(answers);

  // Set Bitmap
  mx.setBuffer(mx.getColumnCount()-1, sizeof(bmp), bmp);
}

void setup() {
  Serial.begin(74880);

  mx.begin();
  mx.control(MD_MAX72XX::INTENSITY, 0);
  mx.control(MD_MAX72XX::UPDATE, MD_MAX72XX::ON);
  mx.clear();

  req.update(); //Initial update to get current time

  Serial.println("Setup complete.");
}

void loop() {
  int answers = req.update();

  Serial.println("New update:");
  for (int i = 0; i < answers; i++)
  {
    char time[20];
    strftime(time, sizeof(time), "%R", localtime(&trans[i].time));
    Serial.printf("%s, in direction %s at %s\r\n", trans[i].name, trans[i].direction, time);
  }
  Serial.println();

  for (size_t i = 0; i < UPDATE_INTERVAL_MIN && answers > 0; i++)
  {
    updateDisplays(answers);
    delay(60*1000);
  }
}
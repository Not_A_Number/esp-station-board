#include "timekeeper.h"

void update_time_offline(tm* updated_time, time_t* last_update_seconds){
  time_t current_time = mktime(updated_time);
  time_t now = millis()/1000;
  current_time += difftime(now, *last_update_seconds);

  // Write updates back to pointers
  localtime_r(&current_time, updated_time);
  *last_update_seconds = now;
}

void parse_dtime(tm* t, const char* date, const char* time){
  const char year[] = {date[0], date[1], date[2], date[3], '\0'}; strptime(year, "%Y", t);
  const char month[] = {date[4], date[5], '\0'}; strptime(month, "%m", t);
  const char day[] = {date[6], date[7], '\0'}; strptime(day, "%d", t);
  const char hour[] = {time[0], time[1], '\0'}; strptime(hour, "%H", t);
  const char minute[] = {time[2], time[3], '\0'}; strptime(minute, "%M", t);
  t->tm_sec = 0;
}


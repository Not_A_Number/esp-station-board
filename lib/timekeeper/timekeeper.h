#include <Arduino.h>
#include <ctime>

void update_time_offline(tm* updated_time, time_t* last_update_seconds);

void parse_dtime(struct tm* t, const char* date, const char* time);
#include <ESP8266WiFi.h>

class requester
{
private:
    const char* station_id;
    int requestedNumberTransporters;
    transporter* trans;
    tm* updated_time;
    time_t* last_update_seconds;
    
    const char* salt = "bdI8UVj40K5fvxwf";
    const char* host = "reiseauskunft.bahn.de"; const int port = 443;
    const char* base_path = "/bin/mgate.exe?checksum=";

    void discardHTTPHeader();

    void generateJson(char* json_request);

    void sendRequest();

    char* saltedHashOf(const char* data);

public:

    WiFiClientSecure wifiClient;

    requester(const char* ssid, const char* password, const char* s_id, int rnt, transporter* t, tm* ut, time_t* lus);

    ~requester();

    int update();
};

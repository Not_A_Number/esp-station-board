#include <ArduinoJson.h>
#include <MD5.h>
#include <timekeeper.h>
#include <transporter.h>

#include <requester.h>

requester::requester(const char* ssid, const char* password, const char* s_id, const int rnt, transporter* t, tm* ut, time_t* lus)
{
    // Init Wifi
    WiFi.mode(WIFI_STA);
    WiFi.begin(ssid, password);
    wifiClient.setInsecure();

    station_id = s_id;
    requestedNumberTransporters = rnt;
    trans = t;
    updated_time = ut;
    last_update_seconds = lus;
}

requester::~requester() = default;

void requester::discardHTTPHeader(){
  const size_t header_len = 500;
  char header[header_len]="";
  while (wifiClient.readBytesUntil('\n', header, header_len) > 1); //Read HTTP Header
}

void requester::generateJson(char* json_request){
  StaticJsonDocument<400> req;
  req["client"] = serialized(R"({"id": "DB", "v": "20100000", "type": "IPH", "name": "DB Navigator"})");
  req["auth"] = serialized(R"({"type": "AID", "aid": "n91dB8Z77MLdoR0K"})");
  req["ext"] = "DB.R21.12.a";
  req["ver"] = "1.15";
  req["svcReqL"][0]["meth"] = "StationBoard";
  req["svcReqL"][0]["req"]["type"] = "DEP"; //Departure

  char id[20];
  strcpy(id, "A=1@L="); strcat(id, station_id); strcat(id, "@");
  req["svcReqL"][0]["req"]["stbLoc"]["lid"] = id;  //Station id

  req["svcReqL"][0]["req"]["dirLoc"] = nullptr; //No direction
  req["svcReqL"][0]["req"]["maxJny"] = requestedNumberTransporters; //Maximum number of journies
  req["svcReqL"][0]["req"]["dur"] = -1; //Time in which trips are searched
  req["svcReqL"][0]["req"]["jnyFltrL"][0] = serialized(R"({"type": "PROD", "mode": "INC", "value": "1023"})"); //Allowed products

  update_time_offline(updated_time, last_update_seconds);
  char date[9], time[7];
  strftime(date, 9, "%Y%m%d", updated_time);
  strftime(time, 7, "%H%M%S", updated_time);
  req["svcReqL"][0]["req"]["date"] = date;
  req["svcReqL"][0]["req"]["time"] = time;
  
  serializeJson(req, json_request, measureJson(req)+1);
}

char* requester::saltedHashOf(const char* data){
  char salted[strlen(data)+strlen(salt)+1];
  strcpy(salted, data);
  strcat(salted, salt);   //add salt
  return MD5::make_digest(MD5::make_hash(salted), 16);
}

void requester::sendRequest(){
  char json_request[600];
  generateJson(json_request);
 
  // put location toghether
  char* hash = saltedHashOf(json_request);
  char path[strlen(base_path) + strlen(hash) + 1];
  strcpy(path, base_path);
  strcat(path, hash);

  //TODO: wait for WiFi connection
  wifiClient.flush();
  wifiClient.connect(host, port);
  //TODO: Test if connection successfull
  wifiClient.print("POST "); wifiClient.print(path); wifiClient.println(" HTTP/1.0");
  wifiClient.print("Host: "); wifiClient.print(host); wifiClient.println();
  wifiClient.print("Content-Length:"); wifiClient.println(strlen(json_request));
  wifiClient.println();
  wifiClient.println(json_request);
}

int requester::update(){
  sendRequest();
  
  discardHTTPHeader();
  
  StaticJsonDocument<500> filter;
  filter["svcResL"][0]["res"]["common"]["prodL"][0]["nameS"] = true;  //Shortnames of Lines
  filter["svcResL"][0]["res"]["jnyL"][0]["prodX"] = true; //Index of line
  filter["svcResL"][0]["res"]["jnyL"][0]["dirTxt"] = true;  //Direction Text
  filter["svcResL"][0]["res"]["jnyL"][0]["stbStop"]["dTimeS"] = true;
  filter["svcResL"][0]["res"]["jnyL"][0]["stbStop"]["dTimeR"] = true;
  filter["svcResL"][0]["res"]["sD"] = true;
  filter["svcResL"][0]["res"]["sT"] = true;

  DynamicJsonDocument doc(3000);
  deserializeJson(doc, wifiClient, DeserializationOption::Filter(filter));
  
  if (doc["svcResL"][0]["res"]["sD"] && doc["svcResL"][0]["res"]["sT"])
  {
    parse_dtime(updated_time, doc["svcResL"][0]["res"]["sD"], doc["svcResL"][0]["res"]["sT"]);
    *last_update_seconds = millis()/1000;
  }
  
  JsonArray jnyL = doc["svcResL"][0]["res"]["jnyL"].as<JsonArray>();

  for (size_t i = 0; i < jnyL.size(); i++)
  {
    // Process name
    strcpy(trans[i].name, doc["svcResL"][0]["res"]["common"]["prodL"][jnyL[i]["prodX"].as<int>()]["nameS"]);

    // Process direction
    strcpy(trans[i].direction, jnyL[i]["dirTxt"]);

    // Process time
    tm t = {0};
    parse_dtime(&t, doc["svcResL"][0]["res"]["sD"], jnyL[i]["stbStop"]["dTimeR"] | jnyL[i]["stbStop"]["dTimeS"]);
    trans[i].time = mktime(&t);
  }

  //Sort by departure
  qsort(trans, jnyL.size(), sizeof(transporter), compare_departures);

  return jnyL.size();
}
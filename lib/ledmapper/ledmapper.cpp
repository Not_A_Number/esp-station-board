#include <Arduino.h>
#include <font8x8_latin.h>
#include <ledmapper.h>


byte getFontLength (utf8_int32_t* c) {
    for (int i = 7; i >= 0 ; i--)
        for (int j = 0; j < 8; j++)
            if (getFontColumn(c, j) >> i)
                return i + 1;
    return 0;
}

byte getFontColumn (utf8_int32_t* c, size_t offset) {
    if (0x0000 <= *c && *c <= 0x007F)
        return font8x8_basic[*c][offset];
    else if(0x00A0 <= *c && *c <= 0x00FF)
        return font8x8_ext_latin[*c - 0x00A0][offset];
    return 0x00;
}
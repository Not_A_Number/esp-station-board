#include <ctime>

struct transporter
{
  char name[20];
  char direction[100];
  time_t time;
};

int compare_departures(const void *t1, const void *t2);
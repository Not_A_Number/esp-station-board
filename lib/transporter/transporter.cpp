#include <transporter.h>

int compare_departures(const void *t1, const void *t2)
{
  double diff = difftime(((struct transporter *)t1)->time, ((struct transporter *)t2)->time);
  if (diff < 0) {
    return -1;
  } else if (diff > 0) {
    return 1;
  } else {
    return 0;
  }
}
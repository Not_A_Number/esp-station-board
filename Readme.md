# ESP Station Board

This Project instructs your ESP8266 to poll the departure times of a public transport station and display them on a LED Matrix.


## Hardware

Additionally to the ESP8266 you need one or multiple daisy-chained LED-Matrices to display the station data. 
I use [MAX7219 FC16](https://www.az-delivery.de/en/products/4-x-64er-led-matrix-display) modules.


## Prerequisites

To find out your station id you'll need [pyHaFAS](https://github.com/FahrplanDatenGarten/pyhafas): 

```shell
pip install pyhafas
```

To upload the program to your ESP8266 you'll need [PlatformIO](https://docs.platformio.org/en/latest//core/installation/index.html):
```shell
python3 -c "$(curl -fsSL https://raw.githubusercontent.com/platformio/platformio/master/scripts/get-platformio.py)"
```


## Setup

After connecting the hardware you'll need to change the [settings.h](include/settings.h) according to your needs.

To get your station id, execute 
```shell
python3 py/getStationId.py <station name>
```

Take a look at [MD_MAX72XX](https://majicdesigns.github.io/MD_MAX72XX/page_connect.html) for hardware options.

After changing at least `ssid`, `password` and `station_id` you are ready to upload the program:
```shell
pio run
```

You can observe the serial monitor through 
```shell
pio device monitor
```
import sys

from pyhafas import HafasClient
from pyhafas.profile import DBProfile

if len(sys.argv) <= 1:
    print(f"Please enter the Station as an Argument: {sys.argv[0]} <station name>")
    exit(1)

client = HafasClient(DBProfile())
for location in client.locations(sys.argv[1]):
    print(f"{location.name}: {location.id}")
